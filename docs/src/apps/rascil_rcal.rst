.. _rascil_apps_rascil_rcal:

.. py:currentmodule:: rascil.apps

.. toctree::
   :maxdepth: 3

===========
rascil_rcal
===========

rascil_rcal is a command line app written using RASCIL. It simulates the real-time
calibration pipeline RCAL. In the SKA, an initial calibration is performed in
real-time as the visibility data are accumulated. An accurate sky model is
assumed to be available or a point source model is used.

In rascil_rcal an MeasurementSet is read in and then iterated through in time-order
solving for the gains. The gaintables are accumulated into a single gain table that is written
as an HDF file. There is also an additional plotting function that plots the gaintable values
(gain amplitude, phase and residual) over time. 

If plotting is required, please make sure you have the correct path --plot_dir set up.

Example script
++++++++++++++

The following runs the real time calibration pipeline on an MS generated by the
MID continuum imaging simulations (with an optional input components file)::

    #!/bin/bash
    python3 $RASCIL/rascil/apps/rascil_rcal.py \
    --ingest_msname SKA_MID_SIM_custom_B2_dec_-45.0_nominal_nchan100_actual.ms \
    --ingest_components_file SKA_MID_SIM_custom_B2_dec_-45.0_nominal_nchan100_components.hdf


Command line arguments
++++++++++++++++++++++

.. argparse::
   :filename: ../../rascil/apps/rascil_rcal.py
   :func: cli_parser
   :prog: rascil_rcal.py
